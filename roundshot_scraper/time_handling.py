from datetime import datetime
from typing import Optional

# all possible formats, that user may pass as format
ALL_FORMATS = {
    'hour': [
        '%H-%M',
        '%H:%M',
    ],
    'day': [
        '%d_%H-%M',
        '%d_%H:%M',

        '%d %H-%M',
        '%d %H:%M',
    ],
    'month': [
        '%m/%d_%H-%M',
        '%m/%d_%H:%M',

        '%m/%d %H-%M',
        '%m/%d %H:%M',

        '%m-%d_%H-%M',
        '%m-%d_%H:%M',

        '%m-%d %H-%M',
        '%m-%d %H:%M',
    ],
    'year': [
        '%Y-%m-%d_%H-%M',
        '%Y-%m-%d_%H:%M',

        '%Y-%m-%d %H-%M',
        '%Y-%m-%d %H:%M',

        '%Y/%m/%d_%H-%M',
        '%Y/%m/%d_%H:%M',

        '%Y/%m/%d %H-%M',
        '%Y/%m/%d %H:%M',
    ],
}


def normalize_datetime(dt: datetime) -> datetime:
    return datetime(
        year=dt.year,
        month=dt.month,
        day=dt.day,
        hour=dt.hour,
        minute=dt.minute - dt.minute % 10,
        second=0,
        microsecond=0,
        tzinfo=None,
    )


def normalized_now() -> datetime:
    """
    normalized now datetime
    (seconds and microseconds are 0, minutes % 10 must be equal to 0)
    :return:
    """
    return normalize_datetime(datetime.now(tz=None))


def parse_raw_time(dt: datetime, raw_time: str) -> Optional[datetime]:
    """
    get a ``datetime``, and change it according to what ``raw_time`` contains
    :param dt: datetime, that will be changed
    :param raw_time: str, raw time (see ALL_FORMATS)
    :return: new datetime
    """
    for type_fmt, fmts in ALL_FORMATS.items():
        for fmt in fmts:
            try:
                new_time = datetime.strptime(raw_time, fmt)
            except ValueError:
                continue
            else:
                dt = dt.replace(
                    year=new_time.year if type_fmt == 'year' else dt.year,
                    month=new_time.month if type_fmt == 'month' else dt.month,
                    day=new_time.day if type_fmt == 'day' else dt.day,
                    hour=new_time.hour if type_fmt == 'hour' else dt.hour,
                    minute=new_time.minute if type_fmt in ['hour', 'day', 'month', 'year'] else dt.minute,
                )
                return dt
    return
